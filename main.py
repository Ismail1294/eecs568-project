import numpy as np
import cv2
import matplotlib.pyplot as plt
import sharedTypes as ST
import sys
import exitScript
import globalLandMarkInit
import maintainTracking
import searchForNewLM
import ekfpredict
import ekfupdate
import media
import graphic
import quat
import csv

def showSearchMasks():
    (v,u,z) = ST.cam.frame.shape
    searchMasks = np.zeros((v,u), dtype=np.uint8)
    for LM in ST.LM_list.active:
        searchMasks = cv2.add(searchMasks, LM.mask*255)
    return searchMasks

def stateMachineRun():
    #map string to function name
    switcher = {
    "Initialize_first_LM": globalLandMarkInit.run,
    "Maintain_tracking": maintainTracking.run,
    "Search_for_new_LM": searchForNewLM.run,
    }
    func = switcher.get(ST.state) #get name of function
    success = func() #execute function, function returns 0 for fail, 1 for success
    return success


def writeToCSV(dataWriter, LMLIST): 
    position = []
    for LM in LMLIST:
        position.append(LM.loc2D[0,0])
        position.append(LM.loc2D[1,0])
    dataWriter.writerow(position)

def dataAppendID(csvfileName, LMLIST_ID):
    with open(csvfileName) as f:
        r = csv.reader(f)
        data = [line for line in r]
    with open(csvfileName,'w') as f:
        w = csv.writer(f)
        w.writerow(LMLIST_ID)
        w.writerows(data)

### Init. Global Variables ###
ST.init()

### Start Video Capture ###
#ST.cam.vidSource = cv2.VideoCapture('./media/ForwardMovement/ForwardMovementID.mp4') 
ST.cam.vidSource = cv2.VideoCapture(media.videoName) 

#ST.cam.vidSource = cv2.VideoCapture(0) #webcam

#Grab one frame to continue init.
ret, ST.cam.frame = ST.cam.vidSource.read()
ST.window = ST.Window()

### Camera Matrix ###
ST.cam.loadCalParameters('./calibration/calibration_data_openGL.npz')

### Init. windows ###
cv2.namedWindow('Camera') #name a window
cv2.namedWindow('Masks')

### Time ###
time_old = 0.0

### Init. State Machine ###
ST.state = "Initialize_first_LM"
print "Starting Global Init. (", ST.state, ")"
graphic.plotInit();

### CSV Writer ###
csvfileName='data.csv'
csvfile = open(csvfileName, 'wb')
dataWriter = csv.writer(csvfile, delimiter=',',quoting=csv.QUOTE_ALL)

### Start MONOSLAM ###
hisLoc = None;
frameNumber = 0
while(ST.cam.vidSource.isOpened()):

#for AA in range(50):
    #Get next frame
    for i in range(2):
        ret, ST.cam.frame = ST.cam.vidSource.read()
    #Get the time
    time_new = ST.cam.vidSource.get(0) #millisec
    time_delta = time_new - time_old
    time_old = time_new
    #Undistort frame of camera
    ST.cam.undistortFrame()

    ### Detect Features #########################

    ### State Machine ###
    #clear the scan list of landmarks
    ST.LM_list.scan = []
    success = stateMachineRun() #perform the function for current state
    if success == True: #go to next state 
        if ST.state == "Initialize_first_LM":
            ST.state = "Maintain_tracking"
            print "Global Init ---> Search for new LM (",ST.state, ")"
        elif ST.state == "Maintain_tracking":
            ST.state = "Search_for_new_LM"
            print "Maintain tracking LM ---> Search for new LM (", ST.state, ")"
        elif ST.state == "Search_for_new_LM":
            ST.state = "Maintain_tracking"
            print "Found new LM ---> Maintain tracking (",ST.state, ")"


    ### Intialize_landmarks_partial##############
    IDs,z = ekfupdate.extract_measurements()
    ekfupdate.check_and_initialize(IDs)
    ### EKF Predict##############################
    ekfpredict.ekfpredict(time_delta)
    if hisLoc == None:
        hisLoc = np.array([ST.mu[0:3]]);
    else:
        hisLoc = np.vstack((hisLoc,ST.mu[0:3]));
        graphic.plotTrajectory(hisLoc);
        print 'History Location:',hisLoc

#    EKF_update()

    ekfupdate.ekfupdate(IDs,z)

    graphic.plotEllipsoid(ST.mu[0:3],ST.Sigma[0:3,0:3],True);
    graphic.plotCamera(ST.mu[0:3],quat.rot(ST.mu[3:7])[:,2]);
    #landMark = [[-3.3,6.05,7.25],[-3.8,-4.2,8.2],[5.9,-3.1,7.5],[3.22,1.2,9.98]]
    for i in range(0,(len(ST.mu)-13)/6):
        cor,cov = ekfupdate.inverseDepthConversion(i);
        #graphic.plotParticles(cor);
        graphic.plotEllipsoid(cor,cov);
        print 'Landmark:',cor,'Covariance:',cov
        print 'Original states:',ST.mu[13+6*i:19+6*i]
    graphic.plotAxis();
    #Display on screen
    
    frameNumber += 1
    cv2.imshow('Camera',ST.cam.frame) # cv2.flip(ST.cam.frame,1))
    cv2.imshow('Masks',showSearchMasks())
    graphic.plotFlip();
    
    writeToCSV(dataWriter, ST.LM_list.allLM)
    #check for ESC key

    controller = True

    if controller == True: 
        while(1):
            ST.key = cv2.waitKey(10) & 0xff #read keyboard, store last 8 binaries  
            if ST.key ==120:
                graphic.rotation[1] += 45;
            if ST.key ==121:
                graphic.rotation[0] += 45;
            if ST.key ==122:
                graphic.rotation[2] += 45;
            if ST.key ==105:
                graphic.zoom += 50;
            if ST.key ==88:
                graphic.rotation[1] -= 45;
            if ST.key ==89:
                graphic.rotation[0] -= 45;
            if ST.key ==90:
                graphic.rotation[2] -= 45;
            if ST.key ==73:
                graphic.zoom -= 50;
            if ST.key ==114:
                graphic.rotation = [0,0,0];
                graphic.zoom = 0;
            if ST.key ==115:
                graphic.screenShotFlag = True;
            if ST.key == 32:
                break
            if ST.key == 27:
                dataAppendID(csvfileName, ST.LM_list.allLMID)
                exitScript.exitScript(ST.cam.vidSource)
            #graphic.plotClose();
                graphic.plotClose();
            #graphic.changeView();
            for i in range(0,(len(ST.mu)-13)/6):
                cor,cov = ekfupdate.inverseDepthConversion(i);
                graphic.plotParticles(cor);
                graphic.plotEllipsoid(cor,cov);
                #print 'Original states:',ST.mu[13+6*i:19+6*i]
            graphic.plotAxis();
            graphic.plotTrajectory(hisLoc);
            graphic.plotEllipsoid(ST.mu[0:3],ST.Sigma[0:3,0:3],True);
            graphic.plotCamera(ST.mu[0:3],quat.rot(ST.mu[3:7])[:,2]);
            graphic.plotFlip();
            graphic.screenShotFlag = False;
    else: 
        ST.key = cv2.waitKey(10) & 0xff #read keyboard, store last 8 binaries  
        if ST.key ==120:
            graphic.rotation[1] += 10;
        if ST.key ==121:
            graphic.rotation[0] += 10;
        if ST.key ==122:
            graphic.rotation[2] += 10;
        if ST.key ==105:
            graphic.zoom += 50;
        if ST.key ==88:
            graphic.rotation[1] -= 10;
        if ST.key ==89:
            graphic.rotation[0] -= 10;
        if ST.key ==90:
            graphic.rotation[2] -= 10;
        if ST.key ==73:
            graphic.zoom -= 50;
        if ST.key ==114:
            graphic.rotation = [0,0,0];
            graphic.zoom = 0;
        if ST.key == 27:
            dataAppendID(csvfileName, ST.LM_list.allLMID)
            exitScript.exitScript(ST.cam.vidSource)
        #graphic.plotClose();
            graphic.plotClose();
        #graphic.changeView();
        for i in range(0,(len(ST.mu)-13)/6):
            cor,cov = ekfupdate.inverseDepthConversion(i);
            #graphic.plotParticles(cor);
            graphic.plotEllipsoid(cor,cov);
            #print 'Original states:',ST.mu[13+6*i:19+6*i]
        graphic.plotAxis();
        graphic.plotTrajectory(hisLoc);
        graphic.plotEllipsoid(ST.mu[0:3],ST.Sigma[0:3,0:3],True);
        graphic.plotCamera(ST.mu[0:3],quat.rot(ST.mu[3:7])[:,2]);
        graphic.plotFlip();



while(1):
    ST.key = cv2.waitKey(10) & 0xff
    if ST.key == 27:
        exitScript.exitScript(ST.cam.vidSource)
        #graphic.plotClose();
        graphic.plotClose();    
    
    #graphic.plotlandmarks([0,5,0],[0,0,1],[0,0,-1000],np.fliplr(ST.cam.frame));
    #graphic.plotFlip();

#exitScript.exitScript(ST.cam.vidSource)
